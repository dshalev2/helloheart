package com.danielshalev.clinicsbe.controllers;

import com.danielshalev.clinicsbe.model.BloodTestResult;
import com.danielshalev.clinicsbe.model.KnownTest;
import com.danielshalev.clinicsbe.services.KnownTestService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BloodTestResultsControllerTest {

    @InjectMocks
    private BloodTestResultsController bloodTestResultsController;

    @Mock
    private KnownTestService knownTestService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        List<KnownTest> knownTestsList= new ArrayList<>();
        knownTestsList.add(new KnownTest("LDL Cholesterol", "ldl", 40));

        Mockito.when(knownTestService.findByTestName(Mockito.matches("ldl"), Mockito.any()))
                .thenReturn(new PageImpl<>(knownTestsList));
        Mockito.when(knownTestService.save(Mockito.any()))
                .thenReturn(new KnownTest());
    }

    @Test
    public void isTestResultAboveThreshold() {
        BloodTestResult ldlAbove = bloodTestResultsController.isTestResultAboveThreshold("ldl", 80);
        Assert.assertEquals("LDL Cholesterol", ldlAbove.getCategory());
        Assert.assertTrue(ldlAbove.getResult());

        BloodTestResult ldlBelow = bloodTestResultsController.isTestResultAboveThreshold("ldl", 30);
        Assert.assertEquals("LDL Cholesterol", ldlBelow.getCategory());
        Assert.assertFalse(ldlBelow.getResult());

        BloodTestResult ldlEquals = bloodTestResultsController.isTestResultAboveThreshold("ldl", 40);
        Assert.assertEquals("LDL Cholesterol", ldlEquals.getCategory());
        Assert.assertFalse(ldlEquals.getResult());
    }
}