package com.danielshalev.clinicsbe.controllers;

import com.danielshalev.clinicsbe.model.BloodTestResult;
import com.danielshalev.clinicsbe.model.KnownTest;
import com.danielshalev.clinicsbe.services.KnownTestService;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class BloodTestResultsController {

    @Autowired
    private KnownTestService knownTestService;

    private boolean initialized = false;

    private void init() {
        Gson gson = new Gson();
        Resource resource = new ClassPathResource("data.json");
        try (JsonReader reader = new JsonReader(new InputStreamReader(resource.getInputStream(), "UTF-8"))) {
            reader.setLenient(true);
            reader.beginArray();
            while (reader.hasNext()) {
                KnownTest knownTest = gson.fromJson(reader, KnownTest.class);
                knownTestService.save(knownTest);
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to init index with dataset");
        }
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/isTestAboveThreshold", method = RequestMethod.GET)
    public BloodTestResult isTestResultAboveThreshold(@RequestParam(value = "test_name") String testName, @RequestParam(value = "value") Integer value) {
        if (!initialized) {
            init();
            initialized = true;
        }
        BloodTestResult result = new BloodTestResult();
        KnownTest knownTest = knownTestService.findByTestName(testName, PageRequest.of(0, 1)).stream().findFirst().orElse(null);
        if (knownTest != null) {
            result.setCategory(knownTest.getCategory());
            result.setResult(value > knownTest.getThreshold());
        }
        return result;
    }

    @RequestMapping(value = "/addKnownTest", method = RequestMethod.POST)
    public void addKnownTest(@RequestBody KnownTest knownTest) {
        knownTestService.save(knownTest);
    }
}
