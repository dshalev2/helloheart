package com.danielshalev.clinicsbe.repository;

import com.danielshalev.clinicsbe.model.KnownTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface KnownTestsRepository extends ElasticsearchRepository<KnownTest, String>{
    @Query(value = "{\"match\" : {\"testName\" :  {\"query\": \"?0\", \"boost\": 1.0, \"fuzziness\": 2, \"prefix_length\": 0, \"max_expansions\": 100}}}")
//    @Query(value = "{\"match\": { \"testName\": \"?0\"}}")
    Page<KnownTest> findByTestName(String testName, Pageable pageable);

    Page<KnownTest> findByTestNameLike(String testName, Pageable pageable);

}
