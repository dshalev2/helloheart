package com.danielshalev.clinicsbe.model;

public class BloodTestResult {
    private String category;
    private boolean result;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
