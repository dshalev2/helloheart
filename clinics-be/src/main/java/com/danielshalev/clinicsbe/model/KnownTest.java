package com.danielshalev.clinicsbe.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import static org.springframework.data.elasticsearch.annotations.FieldIndex.not_analyzed;

@Document(indexName = "known_tests_idx", type = "known_tests")
public class KnownTest {

    @Id
    private String testName;
    private String category;
    private int threshold;

    public KnownTest() {
    }

    public KnownTest(String category, String name, int threshold) {
        this.category = category;
        this.testName = name;
        this.threshold = threshold;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public String toString() {
        return "KnownTest{" +
                ", category='" + category + '\'' +
                ", testName='" + testName + '\'' +
                ", threshold=" + threshold +
                '}';
    }
}
