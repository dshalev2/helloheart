package com.danielshalev.clinicsbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClinicsBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClinicsBeApplication.class, args);
	}
}
