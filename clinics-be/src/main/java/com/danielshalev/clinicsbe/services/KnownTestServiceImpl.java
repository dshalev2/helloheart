package com.danielshalev.clinicsbe.services;

import com.danielshalev.clinicsbe.model.KnownTest;
import com.danielshalev.clinicsbe.repository.KnownTestsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class KnownTestServiceImpl implements KnownTestService {

    @Autowired
    private KnownTestsRepository knownTestsRepository;


    @Override
    public KnownTest save(KnownTest knownTest) {
        return knownTestsRepository.save(knownTest);
    }

    @Override
    public void delete(KnownTest knownTest) {
        knownTestsRepository.delete(knownTest);
    }

    @Override
    public KnownTest findOne(String id) {
        return knownTestsRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<KnownTest> findAll() {
        return knownTestsRepository.findAll();
    }

    @Override
    public Page<KnownTest> findByTestName(String testName, PageRequest pageRequest) {
        return knownTestsRepository.findByTestName(testName, pageRequest);
    }
}
