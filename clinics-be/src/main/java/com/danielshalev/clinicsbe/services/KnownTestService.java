package com.danielshalev.clinicsbe.services;

import com.danielshalev.clinicsbe.model.KnownTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface KnownTestService {

    KnownTest save(KnownTest knownTest);

    void delete(KnownTest knownTest);

    KnownTest findOne(String id);

    Iterable<KnownTest> findAll();

    Page<KnownTest> findByTestName(String testName, PageRequest pageRequest);
}
