import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {HttpClient} from "@angular/common/http";

interface CategoryResult {
  category: string;
  result: boolean;
}

@Component({
selector: 'app-root',
templateUrl: './app.component.html',
styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app';
  categoryResponse: CategoryResult;
  rForm: FormGroup;
  titleAlert:string = 'Field is required';

  constructor(private fb: FormBuilder, private http: HttpClient) {

    this.rForm = fb.group({
      'name' : [null, Validators.required],
      'value' : [null, Validators.required],
      'validate' : ''
    });

  }

  addPost(post) {
    this.categoryResponse = undefined;
    this.http
      .get<CategoryResult>("http://localhost:8080/isTestAboveThreshold?test_name=" + post.name + "&value=" + post.value)
      .subscribe(data => {
        this.categoryResponse = <CategoryResult> data;
      });
  }

  ngOnInit() {
    this.rForm.get('validate').valueChanges.subscribe(

      (validate) => {

        if (validate == '1') {
          this.rForm.get('name').setValidators([Validators.required, Validators.minLength(3)]);
        } else {
          this.rForm.get('name').setValidators(Validators.required);
        }
        this.rForm.get('name').updateValueAndValidity();

      });
  }
}
