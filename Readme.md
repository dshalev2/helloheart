## Clincs.com mini site

### Running Instructions:

#### mac:

1. install homebrew `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
2. install docker `brew tap caskroom/cask; brew cask install docker`
3. clone repository: `git clone https://gitlab.com/dshalev2/helloheart.git`
4. run the following instructions:
	```
	cd helloheart/clinics-be
	./mvnw clean install
	cd ..
	docker-compose down && docker-compose build --no-cache && docker-compose up
	```

#### linux:

##### i have used yum, you may need to change to apt-get or other package manager depending on your distro

1. install docker `sudo yum install docker-ce`
2. start docker `sudo systemctl start docker`
3. clone repository: `git clone https://gitlab.com/dshalev2/helloheart.git`
4. run the following instructions:
	```
	cd helloheart/clinics-be
	./mvnw clean install
	cd ..
	docker-compose down && docker-compose build --no-cache && docker-compose up
	```

#### site will be available at: http://localhost:4200/